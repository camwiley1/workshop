#!/bin/csh
# Your previous .profile  (if any) is saved as .profile.mpsaved
# Setting the path for MacPorts.
echo "reading .profile"
bash -m

##
# Your previous /Users/wileyc1/.profile file was backed up as /Users/wileyc1/.profile.macports-saved_2013-03-26_at_04:16:47
##

# MacPorts Installer addition on 2013-03-26_at_04:16:47: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
#export PATH=/usr/local/php5/bin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.

